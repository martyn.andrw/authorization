OUTPUT_DIR = ./bin
OUTPUT_FILE = app

deps:
	ls go.mod || go mod init gitlab.com/martyn.andrw/authorization
	go mod tidy
	cat go.mod

run: deps
	go run ./...

build: deps
	ls $(OUTPUT_DIR) || mkdir $(OUTPUT_DIR)
	go build -o $(OUTPUT_DIR)/$(OUTPUT_FILE) ./...