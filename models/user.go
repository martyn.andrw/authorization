package models

type User struct {
	ID          int
	Firstname   string
	Lastname    string
	Username    string
	Email       string
	PhoneNumber string

	Permissions map[*Permission]struct{}
	Groups      map[*Group]struct{}
}
