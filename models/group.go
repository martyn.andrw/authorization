package models

type Group struct {
	ID   int
	Name string

	Owner *User

	Users       map[*User]struct{}
	Permissions map[*Permission]struct{}
}
