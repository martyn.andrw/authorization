create database "authorization";

comment on database "authorization" is 'contains user credentials';

create schema credentials;

comment on schema users is 'contains basic user credentials';

create table users.t_user
(
    id           serial      not null
        constraint t_user_pk
            primary key,
    firstname    varchar(20),
    lastname     varchar(20),
    username     varchar(40) not null,
    email        varchar(40),
    phone_number varchar(20)
);

comment on table users.t_user is 'user credential';

create table users.t_permission
(
    id   serial not null
        constraint t_permission_pk
            primary key,
    name varchar(100) not null
);

comment on table users.t_permission is 'permissions';

create table users.t_group
(
    id   serial       not null
        constraint t_group_pk
            primary key,
    name varchar(100) not null
);

create table users.t_user_group
(
    user_id  integer not null
        constraint t_user_group_t_user_id_fk
            references users.t_user,
    group_id integer not null
        constraint t_user_group_t_group_id_fk
            references users.t_group,
    constraint t_user_group_pk
        primary key (user_id, group_id)
);