package logger

import (
	"os"
	"testing"
)

func TestLogger_OK(t *testing.T) {
	l := NewLogger(Default, os.Stderr)
	l.OK("hello", "world")
}

func TestLogger_Info(t *testing.T) {
	l := NewLogger(Default, os.Stderr)
	l.Info("hello", "world")

	l = NewLogger(Info, os.Stderr)
	l.Info("hello", "world 2")
}

func TestLogger_Error(t *testing.T) {
	l := NewLogger(Default, os.Stderr)
	l.Error("err", "!=", "nil")
}
