package service

import (
	"errors"
	"gitlab.com/martyn.andrw/authorization/models"
)

type Service struct {
	repository Repository
	logger     Logger
}

func NewService(repository Repository, logger Logger) *Service {
	return &Service{
		repository: repository,
		logger:     logger,
	}
}

// CreateNewUser creates new user
func (s *Service) CreateNewUser(user *models.User) (int, error) {
	if user.ID != 0 {
		return 0, errors.New("user's id is not nil")
	}
	return s.repository.CreateUser(user)
}

// GetUser returns user by his id
func (s *Service) GetUser(id int) (*models.User, error) {
	if id <= 0 {
		return nil, errors.New("id must be greater than 0")
	}
	return s.repository.GetUser(id)
}

// UpdateUser updates user's info
func (s *Service) UpdateUser(user *models.User) (*models.User, error) {
	if user.ID <= 0 {
		return nil, errors.New("id must be greater than 0")
	}
	return s.repository.UpdateUser(user)
}

// DeleteUser removes user
func (s *Service) DeleteUser(id int) error {
	if id <= 0 {
		return errors.New("id must be greater than 0")
	}
	return s.repository.DeleteUser(id)
}
