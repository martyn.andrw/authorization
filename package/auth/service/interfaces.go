package service

import "gitlab.com/martyn.andrw/authorization/models"

type Repository interface {
	CreateUser(*models.User) (int, error)
	GetUser(int) (*models.User, error)
	UpdateUser(*models.User) (*models.User, error)
	DeleteUser(int) error
}

type Logger interface {
	OK(args ...any)
	Error(args ...any)
	Warn(args ...any)
	Info(args ...any)
	Debug(args ...any)
	Trace(args ...any)
}
